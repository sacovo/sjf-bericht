#!/usr/bin/env python2
from __future__ import print_function
import cv2
import cv2.cv as cv
import sys
import numpy as np
import signal

from thread import start_new_thread
import socket
import time
import datetime

video_writer = cv2.VideoWriter()
fourcc = cv.CV_FOURCC(*'XVID')
title = datetime.datetime.today().strftime('%Y-%m-%d-%H-%M')
video_writer.open('out/' + title + '.avi',fourcc, 48.0, (640, 480))

listening = []
robot_message = [""]
max_empty_frames = 30
empty_frames = 0



def client_thread(conn):
    listening.append(conn)

    for line in line_buffer(conn):
        robot_message[0] = line
        time.sleep(0.5)
    conn.close()

def server_thread():
    HOST = ''
    PORT = 1337
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    s.bind((HOST, PORT))

    s.listen(10)
    while True:
        conn, addr = s.accept()
        start_new_thread(client_thread, (conn,))

start_new_thread(server_thread, ())

cap = cv2.VideoCapture()
cv2.namedWindow(title, 1)

if not cap.open("http://admin:fribot14@192.168.2.10/snapshot.cgi?.mjpg"):
    print("Error opening device")
    sys.exit(-1)


def line_buffer(connection):
    line = ""
    while True:
        try:
            s = connection.recv(1024)
        except:
            return
        for c in s:
            if c == '\n':
                yield line
                line = ""
                continue
            line += c


def frame_iter(video_cap):
    while True:
        success, frame = video_cap.read()
        if success:
            yield frame

def send_message(message):
    for listener in listening:
        try:
            listener.sendall(message)
        except:
            listening.remove(listener)
            listener.close()

last_circle = [-1, -1, 2]

mouse_location = [-1, -1]
goal_location = [-1, -1]

def mouse_callback(event, x, y, flags, param):
    mouse_location[0] = x
    mouse_location[1] = y

    if event == cv2.EVENT_LBUTTONUP:
        goal_location[0] = x
        goal_location[1] = y
        send_message("GOAL: {} {}\n".format(x, y))

cv2.setMouseCallback(title, mouse_callback)

def signal_handler(signal, frame):
    print("Shutdown")
    video_writer.release()
    cap.release()
    cv2.destroyAllWindows()
    sys.exit()


signal.signal(signal.SIGINT, signal_handler)

for frame in frame_iter(cap):
    img = cv2.medianBlur(frame, 5)
    cimg = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    circles = cv2.HoughCircles(cimg, cv.CV_HOUGH_GRADIENT,1,20,
                param1=50,param2=20,minRadius=15,maxRadius=18)

    # cv2.rectangle(img, (0, 360), (230, 480), (255, 255, 255), -1)
    cv2.circle(img, (mouse_location[0], mouse_location[1]), 6, (255, 0, 0), -1)
    font = cv2.FONT_HERSHEY_DUPLEX
    cv2.putText(img, 'Mouse: {:03d}/{:03d}'.format(mouse_location[0], mouse_location[1]),(20,380),font, 0.5, (0, 0, 0), lineType=16)
    if goal_location[0] != -1:
        cv2.circle(img, (goal_location[0], goal_location[1]), 6, (255, 144, 0), -1)
        cv2.putText(img, 'Goal:   {:03d}/{:03d}'.format(goal_location[0], goal_location[1]),(20,400),font, 0.5, (0, 0, 0), lineType=16)
    if listening:
        cv2.putText(img, "Connected", (20, 440), font, 0.5, (0, 150, 0), lineType=16)
        cv2.putText(img, robot_message[0], (20, 460), font, 0.5, (0, 0, 0), lineType=16)
    else:
        cv2.putText(img, "Not connected...", (20, 440), font, 0.5, (0, 0, 255), lineType=16)
    if(circles is None):
        empty_frames += 1
        if max_empty_frames == empty_frames:
            last_circle = [0, 0, 0]
            empty_frames = 0
        elif last_circle[2] != 0:
            send_message("{} {} -1\n".format(last_circle[0], last_circle[1]))
            cv2.circle(img, (last_circle[0], last_circle[1]), last_circle[2], (0, 255 - 10 * empty_frames, 0),2)
            cv2.circle(img, (last_circle[0], last_circle[1]), 2, (0, 0, 255 - 10 * empty_frames), 3)
            cv2.putText(img, 'Robot:  {:03d}/{:03d}'.format(last_circle[0], last_circle[1]),(20,420),font, 0.5, (0, 0, 0), lineType=16)
        else:
            send_message("-1 -1 -1\n")
        cv2.imshow(title, img)
        video_writer.write(img)
        cv2.waitKey(4)
        continue

    circles = np.uint16(np.around(circles))
    for i in circles[0,:]:
        # draw the outer circle
        cv2.circle(img,(i[0],i[1]),i[2],(0,255,0),2)
            # draw the center of the circle
        cv2.circle(img,(i[0],i[1]),2,(0,0,255),3)
        last_circle = [i[0], i[1], i[2]]
        empty_frames = 0
        for listener in listening:
            send_message("{} {} ".format(i[0], i[1]))
    cv2.putText(img, 'Robot:  {:03d}/{:03d}'.format(last_circle[0], last_circle[1]),(20,420),font, 0.5, (0, 0, 0), lineType=16)
    send_message("\n")

    cv2.imshow(title, img)
    video_writer.write(img)
    cv2.waitKey(5)
