/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fribot.cheaps;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import lejos.hardware.BrickFinder;
import lejos.hardware.lcd.TextLCD;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.robotics.MirrorMotor;
import lejos.robotics.RegulatedMotor;
import lejos.robotics.navigation.DifferentialPilot;

import java.awt.Point;

/**
 *
 * @author sandro
 */
public class Robot {

    private RegulatedMotor leftMotor;
    private RegulatedMotor rightMotor;

    private double wheelDist;
    private double wheelDiameter;

    private String cheapsAddress;

    private double wheelCirc;

    private CheaPSUpdater cheaPSUpdater;
    private DisplayUpdater displayUpdater;

    private List<Integer> oldX = new ArrayList<>();
    private List<Integer> oldY = new ArrayList<>();

    private boolean running;

    private double angle;

    private String currentState = "Waiting for Commands...";

    private int x;

    private boolean outOfFrame;
    private boolean turned;
    private int y;

    private double orientation = -1;

    private boolean moving;
    private boolean cancel;

    private DifferentialPilot pilot;
    private TextLCD textLCD = BrickFinder.getDefault().getTextLCD();

    public Robot() {
        this(MirrorMotor.invertMotor(new EV3LargeRegulatedMotor(MotorPort.A)),
                MirrorMotor.invertMotor(new EV3LargeRegulatedMotor(MotorPort.D)),
                12.0, 5.5);
    }

    public Robot(RegulatedMotor leftMotor, RegulatedMotor rightMotor,
            double wheelDist, double wheelDiameter) {
        this.leftMotor = leftMotor;
        this.rightMotor = rightMotor;
        this.wheelDist = wheelDist;
        this.wheelDiameter = wheelDiameter;
        this.wheelCirc = wheelDiameter * Math.PI;

        this.pilot = new DifferentialPilot(wheelDiameter, wheelDist, leftMotor, rightMotor);
        this.pilot.setAcceleration(20);
        this.cheapsAddress = "192.168.2.5";

        this.cheaPSUpdater = new CheaPSUpdater(this);
        this.displayUpdater = new DisplayUpdater(this);
    }

    public void move(int length) {
        getPilot().travel(length);
    }

    public double calcAngle(int x1, int y1, int x2, int y2) {
        return 1.0;
    }

    public void cancelMoving() throws InterruptedException {
        if (!moving) {
            return;
        }
        cancel = true;
        // Wait until the robot isn't moving anymore.
        while (moving) {
            Thread.sleep(100);
        }
        cancel = false;

    }

    private int getSafeX() {
        for (int i = oldY.size() - 1; i >= 0; i--) {
            if (x != -1) {
                return x;
            }
        }

        return -1;
    }

    private int getSafeY() {
        for (int i = oldY.size() - 1; i >= 0; i--) {
            if (oldY.get(i) != -1) {
                return y;
            }
        }

        return -1;
    }

    public void orientateTo(int x, int y, int threshold) {
        int delta = (int) Math.max(Point.distance(this.x, this.y, x, y) / 7, 5);

        if (this.x == -1) {
            pilot.rotate(180);
            int counter = 0;
            while (this.x == -1) {
                pilot.travel(5);
                counter++;
                if (counter == 10) {
                    pilot.rotate(90);
                }
            }
            // Orientatation isn't known anymore...
            this.orientation = -1;
        }
        if (this.orientation == -1) {
            delta = 10;
        } else {
            // atan2(y(C) - y(A),x(C) - x(A)) 180 / π
            int tempX = this.x;
            int tempY = this.y;
            if (tempX == -1) {
                tempX = getSafeX();
                tempY = getSafeY();
            }
            double e = y - tempY;
            double f = x - tempX;

            double etha = this.orientation - Math.toDegrees(Math.atan2(e, f));

            getPilot().rotate(etha);

            this.orientation = Math.toDegrees(Math.atan2(e, f));
        }
        int startX = this.getX();
        int startY = this.getY();

        getPilot().travel(delta);

        int tempX = this.getX();
        int tempY = this.getY();

        double a = Point.distance(tempX, tempY, x, y);
        double b = Point.distance(startX, startY, x, y);
        double c = Point.distance(startX, startY, tempX, tempY);

        if (a < threshold) {
            return;
        }

        double beta = Math.acos((c * c + a * a - b * b) / (2 * a * c));

        double etha;

        double position = (tempX - startX) * (y - startY) - (tempY - startY) * (x - startX);

        if (position < 0) {
            etha = Math.PI - beta;
        } else {
            etha = beta - Math.PI;
        }
        etha = Math.toDegrees(etha);

        // Always use an angle smaller than 180°
        if (Math.abs(etha) >= 180) {
            etha = -(360 - etha);
        }
        // atan2(y(C) - y(A),x(C) - x(A)) 180 / π
        int bd = y - tempY;
        int cd = x - tempX;

        if (startX == -1 | tempX == -1) {
            // Do nothing, since position is wrong...
        } else {
            this.orientation = Math.toDegrees(Math.atan2(bd, cd));
            getPilot().rotate(etha);
        }
    }

    public void moveTo(int x, int y, int precission) {
        setCurrentState("Moving to " + x + "/" + y);
        if (moving) {
            return;
        }

        moving = true;

        while (!cancel) {
            if (Point.distance(x, y, this.x, this.y) > precission) {
                orientateTo(x, y, precission);
            } else {
                setCurrentState("Moved to " + x + "/" + y);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Robot.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        moving = false;
    }

    /**
     * @return the leftMotor
     */
    public RegulatedMotor getLeftMotor() {
        return leftMotor;
    }

    /**
     * @param leftMotor the leftMotor to set
     */
    public void setLeftMotor(RegulatedMotor leftMotor) {
        this.leftMotor = leftMotor;
    }

    /**
     * @return the rightMotor
     */
    public RegulatedMotor getRightMotor() {
        return rightMotor;
    }

    /**
     * @param rightMotor the rightMotor to set
     */
    public void setRightMotor(RegulatedMotor rightMotor) {
        this.rightMotor = rightMotor;
    }

    /**
     * @return the wheelDist
     */
    public double getWheelDist() {
        return wheelDist;
    }

    /**
     * @param wheelDist the wheelDist to set
     */
    public void setWheelDist(double wheelDist) {
        this.wheelDist = wheelDist;
    }

    /**
     * @return the wheelDiameter
     */
    public double getWheelDiameter() {
        return wheelDiameter;
    }

    /**
     * @param wheelDiameter the wheelDiameter to set
     */
    public void setWheelDiameter(double wheelDiameter) {
        this.wheelDiameter = wheelDiameter;
    }

    /**
     * @return the cheapsAddress
     */
    public String getCheapsAddress() {
        return cheapsAddress;
    }

    /**
     * @param cheapsAddress the cheapsAddress to set
     */
    public void setCheapsAddress(String cheapsAddress) {
        this.cheapsAddress = cheapsAddress;
    }

    /**
     * @return the wheelCirc
     */
    public double getWheelCirc() {
        return wheelCirc;
    }

    /**
     * @param wheelCirc the wheelCirc to set
     */
    public void setWheelCirc(double wheelCirc) {
        this.wheelCirc = wheelCirc;
    }

    /**
     * @return the cheaPSUpdater
     */
    public CheaPSUpdater getCheaPSUpdater() {
        return cheaPSUpdater;
    }

    /**
     * @param cheaPSUpdater the cheaPSUpdater to set
     */
    public void setCheaPSUpdater(CheaPSUpdater cheaPSUpdater) {
        this.cheaPSUpdater = cheaPSUpdater;
    }

    /**
     * @return the displayUpdater
     */
    public DisplayUpdater getDisplayUpdater() {
        return displayUpdater;
    }

    /**
     * @param displayUpdater the displayUpdater to set
     */
    public void setDisplayUpdater(DisplayUpdater displayUpdater) {
        this.displayUpdater = displayUpdater;
    }

    /**
     * @return the oldX
     */
    public List<Integer> getOldX() {
        return oldX;
    }

    /**
     * @param oldX the oldX to set
     */
    public void setOldX(List<Integer> oldX) {
        this.oldX = oldX;
    }

    /**
     * @return the oldY
     */
    public List<Integer> getOldY() {
        return oldY;
    }

    /**
     * @param oldY the oldY to set
     */
    public void setOldY(List<Integer> oldY) {
        this.oldY = oldY;
    }

    /**
     * @return the is_running
     */
    public boolean isRunning() {
        return running;
    }

    /**
     * @param isRunning the is_running to set
     */
    public void setRunning(boolean isRunning) {
        this.running = isRunning;
    }

    /**
     * @return the currentState
     */
    public String getCurrentState() {
        return currentState;
    }

    /**
     * @param currentState the currentState to set
     */
    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    /**
     * @return the x
     */
    public int getX() {
        return x;
    }

    /**
     * @param x the x to set
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * @return the y
     */
    public int getY() {
        return y;
    }

    /**
     * @param y the y to set
     */
    public void setY(int y) {
        this.y = y;
    }

    /**
     * @return the pilot
     */
    public DifferentialPilot getPilot() {
        return pilot;
    }

    /**
     * @param pilot the pilot to set
     */
    public void setPilot(DifferentialPilot pilot) {
        this.pilot = pilot;
    }

    /**
     * @return the textLCD
     */
    public TextLCD getTextLCD() {
        return textLCD;
    }

    /**
     * @param textLCD the textLCD to set
     */
    public void setTextLCD(TextLCD textLCD) {
        this.textLCD = textLCD;
    }

}
